"""RealEstate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
# from RealEstateApi import views
from RealEstateApi import viewsOriginal

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     url(r'^getRenters',views.getRentersFromJson),
#     url(r'^getLeaseHolders',views.getLeaseHoldersFromJson),
#     url(r'^getBuyers',views.getBuyersFromJson),
#     url(r'^getSellers', views.getSellersFromJson),
#     url(r'^getBrokers', views.getBrokersFromJson),
#     url(r'^getUserDetail', views.getUserDetail),
#     url(r'^plot', views.showPlot),
# ]

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^getRenters',viewsOriginal.getRenters),
    url(r'^getLeaseHolders',viewsOriginal.getLeaseHolders),
    url(r'^getBuyers',viewsOriginal.getBuyers),
    url(r'^getSellers', viewsOriginal.getSellers),
    url(r'^getBrokers', viewsOriginal.getBrokers),
    url(r'^getUserDetail', viewsOriginal.getUserDetail),
]