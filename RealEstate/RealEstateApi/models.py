from django.db import models

# Create your models here.
class Users(models.Model):
    UserId = models.AutoField(primary_key=True)
    FacebookId = models.CharField(max_length=50)
    Name = models.CharField(max_length=100)
    ProfilePicture = models.CharField(max_length=100)

class TransactionTypes(models.Model):
    TransactionTypeId = models.AutoField(primary_key=True)
    Name = models.CharField(max_length=100)

class Posts(models.Model):
    PostId = models.AutoField(primary_key=True)
    Content = models.TextField()
    PostOwnerId = models.ForeignKey(Users, on_delete=models.CASCADE)
    TransactionTypeId = models.ForeignKey(TransactionTypes, on_delete=models.CASCADE)
    PostUrl = models.TextField()

class PostProducts(models.Model):
    ProductId = models.AutoField(primary_key=True)
    Street = models.TextField()
    District = models.TextField()
    Ward = models.TextField()
    City = models.TextField()
    Area = models.TextField()
    Price = models.TextField()
    PostId = models.ForeignKey(Posts, on_delete=models.CASCADE)

class Comments(models.Model):
    CommentId = models.AutoField(primary_key=True)
    Content = models.TextField()
    CommentOwnerId = models.ForeignKey(Users, on_delete=models.CASCADE)
    PostId = models.ForeignKey(Posts, on_delete=models.CASCADE)
    TransactionTypeId = models.ForeignKey(TransactionTypes, on_delete=models.CASCADE)

class CommentProducts(models.Model):
    ProductId = models.AutoField(primary_key=True)
    Street = models.TextField()
    District = models.TextField()
    Ward = models.TextField()
    City = models.TextField()
    Area = models.TextField()
    Price = models.TextField()
    CommentId = models.ForeignKey(Comments, on_delete=models.CASCADE) 

class Replies(models.Model):
    ReplyId = models.AutoField(primary_key=True)
    Content = models.TextField()
    ReplyOwnerId = models.ForeignKey(Users, on_delete=models.CASCADE)
    CommentId = models.ForeignKey(Comments, on_delete=models.CASCADE) 

class Like(models.Model):
    LikeOwnerId = models.ForeignKey(Users, on_delete=models.CASCADE)
    PostId = models.ForeignKey(Posts, on_delete=models.CASCADE)

class Tagged(models.Model):
    TaggedUserId = models.ForeignKey(Users, on_delete=models.CASCADE)
    CommentId = models.ForeignKey(Comments, on_delete=models.CASCADE)


