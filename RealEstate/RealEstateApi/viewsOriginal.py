from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.core import serializers
from django.conf import settings
import json
import os
import regex
import pandas as pd
from RealEstateApi.models import Users,Like,Posts,Tagged,PostProducts,CommentProducts,TransactionTypes
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.datasets import make_blobs
from sklearn.metrics import silhouette_samples, silhouette_score
import matplotlib.pyplot as plt
import matplotlib.cm as cm
cmap = cm.get_cmap("Spectral")
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
import plotly as py
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, plot

S1 = "ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ"
S0 = "AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy"
def remove_accents(input_str):
    '''Đổi các kí tự Unicode sang dạng không dấu và in thường
    '''
    s = ""
    for c in input_str:
        if c in S1:
            s += S0[S1.index(c)]
        else:
            s += c
    return s.lower()

def silhouetteScorePlot(X, n_clusters):
     # Create a subplot with 1 row and 2 columns
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters)
    cluster_labels = clusterer.fit_predict(X)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)
    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        if i == 3:
            ith_cluster_silhouette_values = ith_cluster_silhouette_values[[122]]

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cmap(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cmap(cluster_labels.astype(float) / n_clusters)
    # ax2.scatter(X.iloc[:, 0], X.iloc[:, 1], marker='.', s=30, lw=0, alpha=0.7,
    #             c=colors, edgecolor='k')
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                    s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    plt.show()

def silhouettePlot(df,k_rng):
    sil = []
    for k in k_rng:
        km = KMeans(n_clusters=k)
        km.fit(df)
        labels = km.labels_
        sil.append(silhouette_score(df, labels, metric = 'euclidean'))
    plt.xlabel('Value of K')
    plt.ylabel('Silhouette score')
    plt.plot(k_rng,sil,marker='o')
    plt.title('The Silhouette method')
    plt.show()

def elbowPlot (df, k_rng):
    sse = []
    for k in k_rng:
        km = KMeans(n_clusters=k)
        km.fit(df)
        sse.append(km.inertia_)
    plt.xlabel('Value of K')
    plt.ylabel('Sum of squared error')
    plt.plot(k_rng,sse,marker='o')
    plt.title('The Elbow method') 
    plt.show()

def clusterResultPlot (df, isBuyer, labels):
    pca_2d = PCA(n_components=2)
    PCs_2d = pd.DataFrame(pca_2d.fit_transform(df.drop(["cluster"], axis=1)))
    PCs_2d.columns = ["PC1_2d", "PC2_2d"]
    plotX = pd.concat([df,PCs_2d], axis=1, join='inner')
    cluster0 = plotX[plotX["cluster"] == 0]
    cluster1 = plotX[plotX["cluster"] == 1]
    cluster2 = plotX[plotX["cluster"] == 2]
    cluster3 = None
    if isBuyer:
        cluster3 = plotX[plotX["cluster"] == 3]
    trace1 = go.Scatter(
                        x = cluster0["PC1_2d"],
                        y = cluster0["PC2_2d"],
                        mode = "markers",
                        name = labels[0],
                        marker = dict(color = 'rgba(255, 128, 255, 0.8)'),
                        text = None)
    trace2 = go.Scatter(
                        x = cluster1["PC1_2d"],
                        y = cluster1["PC2_2d"],
                        mode = "markers",
                        name = labels[1],
                        marker = dict(color = 'rgba(255, 128, 2, 0.8)'),
                        text = None)
    trace3 = go.Scatter(
                        x = cluster2["PC1_2d"],
                        y = cluster2["PC2_2d"],
                        mode = "markers",
                        name = labels[2],
                        marker = dict(color = 'rgba(0, 255, 200, 0.8)'),
                        text = None)
    trace4 = None
    if isBuyer:
        trace4 = go.Scatter(
                            x = cluster3["PC1_2d"],
                            y = cluster3["PC2_2d"],
                            mode = "markers",
                            name = labels[3],
                            marker = dict(color = 'rgba(0, 0, 0, 1)'),
                            text = None)
    if isBuyer:
        data = [trace1, trace2, trace3,trace4]
    else:
        data = [trace1, trace2, trace3]
    title = "Visualizing Clusters in Two Dimensions Using PCA"
    layout = dict(title = title,
                xaxis= dict(title= 'PC1',ticklen= 5,zeroline= False),
                yaxis= dict(title= 'PC2',ticklen= 5,zeroline= False)
                )
    fig = dict(data = data, layout = layout)
    plot(fig)

re_tag = r"thamkhao(di|nhe|thu|nha)|(xem|coi)(ne|nha|nhe|thu)|(xem|coi)(duoc)(khong|ko|k|kg|hong|hem|hom)|(dicoi)(khong|ko|k|kg|hong|hom|hem)|tag.+vao|ne(ba|ma|bo|may)|(thamkhao|.+)(ne|kia)|quacoidi"
re_inb = r"tuvan|inbox|inb|ib|ibx|inboxx|check(inbox|ib|tn)|tv(giup|gium|nha|nhe)|tv(minh|chi|em|anh|chu|co|toi|dum)|cantuvan|cantv|guitinnhanrieng|tinnhancho"
re_inb_num = r"\d{9,12}"
re_inb_email = r'''(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'''
re_price = r"cho.*(hoi)*gia|^gia$|^.+vagia$|^bn$|^bn\?$|^nhiu$|gia(ngay)*(rasao|thenao|sao|ntn)*(ad|a|b)|giaca(rasao|thenao)|bn(ad|a|va)|gia(thue)*(p|phong|ca|tien|ban|1m|m2|1met|thang|1thang|1phong|1p)*(thenao|nhuthenao|tn|ntn|tnao|nhiu|nhieu|sao|s|di|nhunao|bn|bnhieu)|(xin|cho|bao).*gia|(ban)*(baonhieu|baonhiu|bnhju|bnhiu|bn|baotien|nhiu|nhieu|bnh)(1m|m2|1met|thang|1thang|1phong|1p|1ngay)|baonhieu|baonhiu|bnhju|bnhiu|baotien|(nhieu|nhiu)(z|vay|va)|(inbox|inb|ib|ibox|inbx|ibx).*(giá|gia|giad)|baotiền|price|(cannay|phong|nha)(ban)*(baonhieu|bn|nhieu|baonhiu|bnhju|bnhiu|baotien|nhiu|bnh)|gia.*(chinhxac|dung|chot)chua|gianhutrenhaysao|cogiaban(canho|nha).*(ko|khong|k)"
re_info = r"^thongtin$|(cho|xin|ib|inbox|ibox)*(thongtin).*(nha|phong)|(xin|can|gui|cho|ib|inbox|ibox|inb|ibx).*(thongtin|info|thoongtin|chitiet)|(xin|can|gui|cho|ib|inbox|ibox|inb|ibx)(em)*tt|datgivay|(hembetong)(ko|khong|k|kg|hong)|(ib|inbox|ibox|inb)*(giup)*(co)*(quyhoach).*(ko|khong|k|kg|hong)|(1|phong)(baonguoi|maynguoi)|kinhdoanhgi(khong)*|(phong)*(cho|ko)*(nauan|nauaqn)(dk|ko|k)|\d+(ng|nguoi)(o)*(duoc|dc)(kh|khong|ko|hk)|gio.*tudok|(nuoi|nui)(pet|cho|meo|cun)(dc|duoc)*(k|khong|ko)|codinhlogioikhong|hem.*(tn|tnao|ntn|thenao)|xe.*vonhaduoc(kg|khong)|hem(baonhieu|bn|nhieu|baonhiu)(met|m2)"
re_orientation = r"(diachi|dchi)|(nha|phong)*(odau|0dau|chonao|quanmay|quannao|huongnao|duongnao|khuvucnao|khucnao|phuongnao|haydauvay)|(nha|phong)(dau)|(diachi|dchi).*(dauvay|cuthe|baonhieu|odau|bn)|laumay|trenlauha|(cho|xin|gui).*(dchi|diachi|vitri)|chungcutengi|(inbox|ib).*(diachi|dchi|vtri|vitri)|thuoc.*(dungkhong|dungk)"
re_infrastructure = r"dientich|(dientich|dt)(baonhieu|baonhiu|bnhiu|bnhieu|bn|thenao|nhuthenao|tn|tnao|ntn)|(co)\d+(can|phongngu)|(may|baonhieu|baonhiu|bnhiu|bnhieu|bn)(phongngu|pn|m2|met|m|tang|lau|phong)|noithathaynhatrong|nhatronghayconoithat|(inbox|inb|ib|ibx|inboxx|xin)(dientich|dt)|(xecon|ot)(vo|do|vao|ravao|di)(duoc)(khong|ko|k|hong|kg)|co(bep|nhabep|bancong|banbep|maygiat|maylanh|kebep)(khong|kg|k|ko)*|co(wc|nhavesinh)rienghk|conoithat(khong|ko)|gaccao(baonhieu|baonhiu|bnhiu|bnhieu|bn)|toletchungha|toiletchunghayrieng|xin.+dientich|daydutiennghi.*(ko|khong|k)|tuongchunghayrieng|(dai|ngang).*(tn|tnao|ntn|thenao)|diennuoc(thenao|sao|ao)|(ko|khong)co(noithat).*a"
re_image = r"(cho)*(xin|xem|coi|xem).*(anh|hinhanh|hinh)|(khong|ko|k)(co)(anh|hinhảnh|anhchup)(a|ah|ha|sao)|co(anh|hinhanh|hinh).*(khong|ko|k)|(inbox|inb|ib|ibx|inboxx)(cho)*(em|minh)*(anh|hinhanh|hinh)"
re_bargain = r"(giam|cobot)(khong|ko|k|hong|kg)|(khong|ko|k|hong|kg)(giam|bot|ha)(a|ah|ha|sao)|\d+(tr|trieu|ty|ti)\d*(dc|duoc)(hok|khong|ko|k|kg)|chot\d+(tr|trieu|ty|ti)\d*|\d+(tr|trieu)quaydau|(gia)*conthuongluong(khong|ko)"
re_payment = r"(tragop|thanhtoan|pttt)(sao|thenao|nhuthenao|ntn|tn)|(duatruoc|tratruoc)(bnhiu|bnhiu|bnhieu|bn)|(tra|cho)(gop)(khong|ko|kg|k|hong|hom|hem)|(gop)(baonhieu|baonhiu|bnhiu|bnhieu|bn|nhuthenao|thenao|ntn|tn)|(muongop|mungop)(sao|nhuthenao|ntn|thenao|tn|lamgi)|coc.*(maythang|baonhieu)|hotrotragop(khong)*|\d+.*laduatruoc(haysao|haylasao)|(tratruoc|trasau)hay(trasau|tratruoc)*"
re_red = r"(xem|coi|thamkhao|gui|goi|cho).*(so|giayto)|(inbox|ib|ibx|ip|inb|jbx).*(so|giayto)|(co|ra)(so|sohong)(khong|ko|k|kg|chua)|(so|sohong|s)(rieng|chung|hr)hay(chung|rieng|roeng)|(so|giayto)(thenao|nhuthenao|ntn|svay|saovay|sao)|(sogi)(ah|a|anh|em|e|ban|b|ad)|xin.*(giayto|so|banve)|chuphinhso|so(chung|rieng)(phai)*(khong|kh|pk)|(sosach|so|sohong|giaytay).*(rasao|sao|haysao)|so(chung|rieng)(chacluon)|so.*lavibang(hay)*|sohayvibang|giay.*vibang|vibang(ha|phaikhong|dungkhong|dungko)"
re_red_exception = r"((xem|coi|thamkhao|gui|goi|cho)\w*(so)|(inbox|ib|ibx|ip|inb)\w*(so)|xin\w*so)(dienthoai|dt|diachi|phone|didong|dchi|dien)"
re_visit = r"(khinao|lucnao|baogio|chugnao).*(dixem|xem|thamquan|thamquan).*(dx|duoc|dc)|(muon|mun)(thamquan|xem)|(cho)(datlich)(xem|coi|thamquan)(duocko|duockhong|dcko|dck)*|(dixem|quacoi|xemdat|coiphong).*(duockhong|dck)|(can)*.*xem(duanthucte|phong|nha)|(den|di|qua)xem(duockhong|dck|dc)*"
re_status = r"(khinao|baogio|thangmay|lucnao).*(xong|hoanthien|bangiao|tranha|moban|moigiao|oduoc|odc)|(xay|hoanthien|xong|conha|olien|hinhthanh|colien|laylien)(chua)|(olien)(duoc|dc|đc)(khong|ko|k|kg|hong)|(con|co|ban|coban)(phong|nha|can|chomuon).*(khong|ko|kg|k|hong|hom|hem|chua|chuea|kh|nao)|con(khong|ko|kg|k|hong|hom|hem)|ban(chua|chuea)|(phong|nha|canho|can|dat).*con(khong|ko|kg|k|hong|hom|hem|a)|co(ng|nguoi)(thue|muon)chua|co.*((q|quan)\d{1,2}|binhthanh|tanbinh|phunhuan|binhtan).*(khong|ko|kg|k|hong|hom|hem|kh)|((q|quan)\d{1,2}|binhthanh|tanbinh|phunhuan|binhtan).*(cok|cokhong)"
re_project = r"(ban)*(canho|chungcu|cc|c.ho|dat|lienke|nhapho|nguyencan|nharieng)hay.*(dat|canho|chungcu|cc|c.ho|lienke|nhapho|nharieng|nhalienke|day|nguyennhatro|nha|duan)|(nhalienke|chungcu|nhapho|cc|c.ho|canho)(ha|phaikhong|pk)|(oghep)hay(sao|thuerieng)"

renterOut = ['100009066242639','100004978242703', '100015456288052','100000001074353','100042915381926','100007003956913','1538798668','100042137239908','100043461920282','100013118629630','100004230191360','100002837445813','100025737065103']
buyerOut = ['100015407685735','100024812480981','100024541639595']
# Create your views here.
clusterNormal = 'Khách không tương tác'
clusterRenter0dot0 = 'Khách ít tương tác'
clusterRenter0dot1 = 'Khách hay inbox riêng'
clusterRenter0dot3 = 'Khách hay hỏi xin hình phòng'
clusterRenter1 = 'Khách hay hỏi giả, hỏi thông tin phòng và inbox riêng'
clusterRenter2 = 'Khách tương tác bình luận nhiều'
@api_view(["GET"])
def getRenters(request):
    try:
        renters = []
        users = Users.objects.all()
        for user in users:
            nextUser = False
            isRenter = False
            for post in user.posts_set.values():
                if post['TransactionTypeId_id'] == 4:
                    isRenter = False
                    nextUser = True
                    break
                if post['TransactionTypeId_id'] == 3:
                    isRenter = True
            if(nextUser == True):
                continue
            for comment in user.comments_set.values():
                if comment['TransactionTypeId_id'] == 4:
                    isRenter = False
                    break
                if comment['TransactionTypeId_id'] == 3:
                    isRenter = True
            if isRenter:
                renters.append(user)
        potentialRenters = []
        normalRenterFaceIds = []
        for renter in renters:
            if renter.FacebookId in renterOut:
                continue
            commentWithIntendCount = 0
            for renterComment in renter.comments_set.values():
                if renterComment['TransactionTypeId_id'] != 5:
                    commentWithIntendCount += 1
            haveLikeLeasePosts = False
            for like in Like.objects.filter(LikeOwnerId = renter.UserId):
                if like.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                    haveLikeLeasePosts = True
                    break
            haveTaggedLeasePosts = False
            for tagged in Tagged.objects.filter(TaggedUserId = renter.UserId):
                if tagged.CommentId.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                    haveTaggedLeasePosts = True
                    break
            if (len(renter.comments_set.values()) > 0 and len(renter.comments_set.values()) > commentWithIntendCount) or haveLikeLeasePosts or haveTaggedLeasePosts or len(renter.replies_set.values()) > 0:
                potentialRenters.append(renter)
            else:
                normalRenterFaceIds.append(renter.FacebookId)
        userFeature = []
        cmtTagFeature = []
        cmtInboxFeature = []
        cmtPriceFeature = []
        cmtInfoFeature = []
        cmtImgFeature = []
        cmtRedFeature = []
        cmtVisitFeature = []
        cmtStatusFeature = []
        likeFeature = []
        taggedFeature = []
        for potentialRenter in potentialRenters:
            cmtTagCount = 0
            cmtInboxCount = 0
            cmtPriceCount = 0
            cmtInfoCount = 0
            cmtImgCount = 0
            cmtRedCount = 0
            cmtVisitCount = 0
            cmtStatusCount = 0
            likeLeasePosts = 0
            taggedLeasePosts = 0
            userFeature.append(potentialRenter.FacebookId)
            for potentialRenterComment in potentialRenter.comments_set.values():
                if potentialRenterComment['TransactionTypeId_id'] != 5:
                    continue
                text = remove_accents(potentialRenterComment['Content']).replace(" ","").replace("\xa0","")
                try:
                    taggedComment = Tagged.objects.get(CommentId = potentialRenterComment['CommentId'])
                except Tagged.DoesNotExist:
                    taggedComment = None
                if taggedComment:
                    cmtTagCount += 1
                if regex.findall(re_inb,text) or regex.findall(re_inb_num,text) or regex.findall(re_inb_email,text) :
                    cmtInboxCount += 1
                if regex.findall(re_price,text):
                    cmtPriceCount += 1
                if regex.findall(re_info,text) or regex.findall(re_orientation,text) or regex.findall(re_infrastructure,text):
                    cmtInfoCount += 1
                if regex.findall(re_image,text):
                    cmtImgCount += 1
                if regex.findall(re_visit,text):
                    cmtVisitCount += 1
                if regex.findall(re_status,text):
                    cmtStatusCount += 1
            for potentialRenterReply in potentialRenter.replies_set.values():
                text = remove_accents(potentialRenterReply['Content']).replace(" ","").replace("\xa0","")
                if regex.findall(re_inb,text) or regex.findall(re_inb_num,text) or regex.findall(re_inb_email,text) :
                    cmtInboxCount += 1
                if regex.findall(re_price,text):
                    cmtPriceCount += 1
                if regex.findall(re_info,text) or regex.findall(re_orientation,text) or regex.findall(re_infrastructure,text):
                    cmtInfoCount += 1
                if regex.findall(re_image,text):
                    cmtImgCount += 1
                if regex.findall(re_visit,text):
                    cmtVisitCount += 1
                if regex.findall(re_status,text):
                    cmtStatusCount += 1
            for like in Like.objects.filter(LikeOwnerId = potentialRenter.UserId):
                if like.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                    likeLeasePosts += 1
            for tagged in Tagged.objects.filter(TaggedUserId = potentialRenter.UserId):
                if tagged.CommentId.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                    taggedLeasePosts += 1
            cmtTagFeature.append(cmtTagCount)
            cmtInboxFeature.append(cmtInboxCount)
            cmtPriceFeature.append(cmtPriceCount)
            cmtInfoFeature.append(cmtInfoCount)
            cmtImgFeature.append(cmtImgCount)
            cmtRedFeature.append(cmtRedCount)
            cmtVisitFeature.append(cmtVisitCount)
            cmtStatusFeature.append(cmtStatusCount)
            likeFeature.append(likeLeasePosts)
            taggedFeature.append(taggedLeasePosts)
        feature_dict = {
            'users': userFeature,
            'cmtTag': cmtTagFeature,
            'cmtInbox': cmtInboxFeature,
            'cmtPrice': cmtPriceFeature,
            'cmtInfo': cmtInfoFeature,
            'cmtImg': cmtImgFeature,
            # 'cmtRed': cmtRedFeature,
            'cmtVisit': cmtVisitFeature,
            'cmtStatus': cmtStatusFeature,
            'likes': likeFeature,
            'taggedFeature': taggedFeature
        }
        df = pd.DataFrame(feature_dict)
        km = KMeans(n_clusters = 3)
        predicts = km.fit_predict(df[['cmtTag', 'cmtInbox', 'cmtPrice', 'cmtInfo','cmtImg','cmtVisit','cmtStatus', 'likes', 'taggedFeature']])
        centersOrder = np.argsort(km.cluster_centers_.sum(axis=1))
        group = []
        for predict in predicts:
            order = centersOrder.tolist().index(predict)
            group.append(order)
        df['cluster'] = group

        df0 = df.loc[df['cluster'] == 0]
        km0 = KMeans(n_clusters = 4)
        predicts0 = km0.fit_predict(df0[['cmtTag', 'cmtInbox', 'cmtPrice', 'cmtInfo','cmtImg','cmtVisit','cmtStatus', 'likes', 'taggedFeature']])
        centersOrder0 = np.argsort(km0.cluster_centers_.sum(axis=1))
        group0 = []
        for predict in predicts0:
            order = centersOrder0.tolist().index(predict)
            group0.append(order)
        df0['cluster'] = group0

        result = []
        index0 = 0
        for index, renterFacebook in enumerate(userFeature):
            renterObj = {}
            renterObj['facebookId'] = renterFacebook
            if group[index] == 0:
                if group0[index0] == 0:
                    renterObj['cluster'] = clusterRenter0dot0
                if group0[index0] == 1:
                    renterObj['cluster'] = clusterRenter0dot1
                if group0[index0] == 2:
                    renterObj['cluster'] = clusterRenter1
                if group0[index0] == 3:
                    renterObj['cluster'] = clusterRenter0dot3
                index0 += 1
            if group[index] == 1:
                renterObj['cluster'] = clusterRenter1
            if group[index] == 2:
                renterObj['cluster'] = clusterRenter2
            result.append(renterObj)
        for normalRenterFacebook in normalRenterFaceIds:
            renterObj = {}
            renterObj['facebookId'] = normalRenterFacebook
            renterObj['cluster'] = clusterNormal
            result.append(renterObj)
        return JsonResponse(result,safe=False)  
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)

@api_view(["GET"])
def getLeaseHolders(request):
    try:
        leaseHolders = []
        users = Users.objects.all()
        for user in users:
            nextUser = False
            isLeaseHolder = False
            for post in user.posts_set.values():
                if post['TransactionTypeId_id'] == 3:
                    isLeaseHolder = False
                    nextUser = True
                    break
                if post['TransactionTypeId_id'] == 4:
                    isLeaseHolder = True
            if(nextUser == True):
                continue
            for comment in user.comments_set.values():
                if comment['TransactionTypeId_id'] == 3:
                    isLeaseHolder = False
                    break
                if comment['TransactionTypeId_id'] == 4:
                    isLeaseHolder = True
            if isLeaseHolder:
                leaseHolders.append(user.FacebookId)
        result = []
        for holder in leaseHolders:
            holderObj = {'facebookId': holder}
            result.append(holderObj)
        return JsonResponse(result,safe=False)
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)

clusterBuyer0 = 'Khách ít tương tác'
clusterBuyer1dot0 = 'Khách hay inbox riêng'
clusterBuyer1dot3 = 'Khách hay hỏi giả, hỏi thông tin phòng và inbox riêng'
clusterBuyer1dot4 = 'Khách chủ yếu like các bài post bán nhà'
clusterBuyer1dot5 = 'Khách hay hỏi giá và inbox riêng'
clusterBuyer2 = 'Khách hay inbox riêng và yêu cầu xem sổ hồng'
clusterBuyer3 = 'Khách tương tác bình luận nhiều'
@api_view(["GET"])
def getBuyers(request):
    try:
        buyers = []
        users = Users.objects.all()
        for user in users:
            nextUser = False
            isBuyer = False
            for post in user.posts_set.values():
                if post['TransactionTypeId_id'] == 2:
                    isBuyer = False
                    nextUser = True
                    break
                if post['TransactionTypeId_id'] == 1:
                    isBuyer = True
            if(nextUser == True):
                continue
            for comment in user.comments_set.values():
                if comment['TransactionTypeId_id'] == 2:
                    isBuyer = False
                    break
                if comment['TransactionTypeId_id'] == 1:
                    isBuyer = True
            if isBuyer:
                buyers.append(user)
        potentialBuyers = []
        normalBuyerFaceIds = []
        for buyer in buyers:
            if buyer.FacebookId in buyerOut:
                continue
            commentWithIntendCount = 0
            for buyerComment in buyer.comments_set.values():
                if buyerComment['TransactionTypeId_id'] != 5:
                    commentWithIntendCount += 1
            haveLikeSellPosts = False
            for like in Like.objects.filter(LikeOwnerId = buyer.UserId):
                if like.PostId.TransactionTypeId.Name == 'Bán nhà':
                    haveLikeSellPosts = True
                    break
            haveTaggedSellPosts = False
            for tagged in Tagged.objects.filter(TaggedUserId = buyer.UserId):
                if tagged.CommentId.PostId.TransactionTypeId.Name == 'Bán nhà':
                    haveTaggedSellPosts = True
                    break
            if (len(buyer.comments_set.values()) > 0 and len(buyer.comments_set.values()) > commentWithIntendCount) or haveLikeSellPosts or  haveTaggedSellPosts or len(buyer.replies_set.values()) > 0:
                potentialBuyers.append(buyer)
            else:
                normalBuyerFaceIds.append(buyer.FacebookId)
        userFeature = []
        cmtTagFeature = []
        cmtInboxFeature = []
        cmtPriceFeature = []
        cmtInfoFeature = []
        cmtImgFeature = []
        cmtRedFeature = []
        cmtVisitFeature = []
        cmtStatusFeature = []
        likeFeature = []
        taggedFeature = []
        for potentialBuyer in potentialBuyers:
            cmtTagCount = 0
            cmtInboxCount = 0
            cmtPriceCount = 0
            cmtInfoCount = 0
            cmtImgCount = 0
            cmtRedCount = 0
            cmtVisitCount = 0
            cmtStatusCount = 0
            likeSellPosts = 0
            taggedSellPosts = 0
            userFeature.append(potentialBuyer.FacebookId)
            for potentialBuyerComment in potentialBuyer.comments_set.values():
                if potentialBuyerComment['TransactionTypeId_id'] != 5:
                    continue
                text = remove_accents(potentialBuyerComment['Content']).replace(" ","").replace("\xa0","")
                try:
                    taggedComment = Tagged.objects.get(CommentId = potentialBuyerComment['CommentId'])
                except Tagged.DoesNotExist:
                    taggedComment = None
                if taggedComment:
                    cmtTagCount += 1
                if regex.findall(re_inb,text) or regex.findall(re_inb_num,text) or regex.findall(re_inb_email,text) :
                    cmtInboxCount += 1
                if regex.findall(re_price,text):
                    cmtPriceCount += 1
                if regex.findall(re_info,text) or regex.findall(re_orientation,text) or regex.findall(re_infrastructure,text):
                    cmtInfoCount += 1
                if regex.findall(re_image,text):
                    cmtImgCount += 1
                if regex.findall(re_visit,text):
                    cmtVisitCount += 1
                if regex.findall(re_status,text):
                    cmtStatusCount += 1
            for potentialBuyerReply in potentialBuyer.replies_set.values():
                text = remove_accents(potentialBuyerReply['Content']).replace(" ","").replace("\xa0","")
                if regex.findall(re_inb,text) or regex.findall(re_inb_num,text) or regex.findall(re_inb_email,text) :
                    cmtInboxCount += 1
                if regex.findall(re_price,text):
                    cmtPriceCount += 1
                if regex.findall(re_info,text) or regex.findall(re_orientation,text) or regex.findall(re_infrastructure,text):
                    cmtInfoCount += 1
                if regex.findall(re_image,text):
                    cmtImgCount += 1
                if regex.findall(re_visit,text):
                    cmtVisitCount += 1
                if regex.findall(re_status,text):
                    cmtStatusCount += 1
            for like in Like.objects.filter(LikeOwnerId = potentialBuyer.UserId):
                if like.PostId.TransactionTypeId.Name == 'Bán nhà':
                    likeSellPosts += 1
            for tagged in Tagged.objects.filter(TaggedUserId = potentialBuyer.UserId):
                if tagged.CommentId.PostId.TransactionTypeId.Name == 'Bán nhà':
                    taggedSellPosts += 1
            cmtTagFeature.append(cmtTagCount)
            cmtInboxFeature.append(cmtInboxCount)
            cmtPriceFeature.append(cmtPriceCount)
            cmtInfoFeature.append(cmtInfoCount)
            cmtImgFeature.append(cmtImgCount)
            cmtRedFeature.append(cmtRedCount)
            cmtVisitFeature.append(cmtVisitCount)
            cmtStatusFeature.append(cmtStatusCount)
            likeFeature.append(likeSellPosts)
            taggedFeature.append(taggedSellPosts)
        feature_dict = {
            'users': userFeature,
            'cmtTag': cmtTagFeature,
            'cmtInbox': cmtInboxFeature,
            'cmtPrice': cmtPriceFeature,
            'cmtInfo': cmtInfoFeature,
            'cmtImg': cmtImgFeature,
            'cmtRed': cmtRedFeature,
            'cmtVisit': cmtVisitFeature,
            'cmtStatus': cmtStatusFeature,
            'likes': likeFeature,
            'taggedFeature': taggedFeature
        }
        df = pd.DataFrame(feature_dict)
        km = KMeans(n_clusters = 4)
        predicts = km.fit_predict(df[['cmtTag', 'cmtInbox', 'cmtPrice', 'cmtInfo','cmtImg','cmtRed','cmtVisit','cmtStatus', 'likes', 'taggedFeature']])
        centersOrder = np.argsort(km.cluster_centers_.sum(axis=1))
        group = []
        for predict in predicts:
            order = centersOrder.tolist().index(predict)
            group.append(order)
        df['cluster'] = group

        df1 = df.loc[df['cluster'] == 1]
        km1 = KMeans(n_clusters = 6)
        predicts1 = km1.fit_predict(df1[['cmtTag', 'cmtInbox', 'cmtPrice', 'cmtInfo','cmtImg','cmtRed','cmtVisit','cmtStatus', 'likes', 'taggedFeature']])
        centersOrder1 = np.argsort(km1.cluster_centers_.sum(axis=1))
        group1 = []
        for predict in predicts1:
            order = centersOrder1.tolist().index(predict)
            group1.append(order)
        df1['cluster'] = group1

        result = []
        index1 = 0
        for index, buyerFacebook in enumerate(userFeature):
            buyerObj = {}
            buyerObj['facebookId'] = buyerFacebook
            if group[index] == 0:
                buyerObj['cluster'] = clusterBuyer0
            if group[index] == 1:
                if group1[index1] == 0:
                    buyerObj['cluster'] = clusterBuyer1dot0
                if group1[index1] == 1:
                    buyerObj['cluster'] = clusterBuyer2
                if group1[index1] == 2:
                    buyerObj['cluster'] = clusterBuyer1dot0
                if group1[index1] == 3:
                    buyerObj['cluster'] = clusterBuyer1dot3
                if group1[index1] == 4:
                    buyerObj['cluster'] = clusterBuyer1dot4
                if group1[index1] == 5:
                    buyerObj['cluster'] = clusterBuyer1dot5
                index1 += 1
            if group[index] == 2:
                buyerObj['cluster'] = clusterBuyer2
            if group[index] == 3:
                buyerObj['cluster'] = clusterBuyer3
            result.append(buyerObj)
        for normalBuyerFacebook in normalBuyerFaceIds:
            buyerObj = {}
            buyerObj['facebookId'] = normalBuyerFacebook
            buyerObj['cluster'] = clusterNormal
            result.append(buyerObj)
        return JsonResponse(result,safe=False)  
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)

@api_view(["GET"])
def getSellers(request):
    try:
        sellers = []
        users = Users.objects.all()
        for user in users:
            nextUser = False
            isSeller = False
            for post in user.posts_set.values():
                if post['TransactionTypeId_id'] == 1:
                    isSeller = False
                    nextUser = True
                    break
                if post['TransactionTypeId_id'] == 2:
                    isSeller = True
            if(nextUser == True):
                continue
            for comment in user.comments_set.values():
                if comment['TransactionTypeId_id'] == 1:
                    isSeller = False
                    break
                if comment['TransactionTypeId_id'] == 2:
                    isSeller = True
            if isSeller:
                sellers.append(user.FacebookId)
        result = []
        for seller in sellers:
            sellerObj = {'facebookId': seller}
            result.append(sellerObj)
        return JsonResponse(result,safe=False)
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)

@api_view(["GET"])
def getBrokers(request):
    try:
        brokers = []
        users = Users.objects.all()
        for user in users:
            isSeller = False
            isBuyer = False
            for post in user.posts_set.values():
                if post['TransactionTypeId_id'] == 1:
                    isBuyer = True
                if post['TransactionTypeId_id'] == 2:
                    isSeller = True
            if not isBuyer or not isSeller:         
                for comment in user.comments_set.values():
                    if comment['TransactionTypeId_id'] == 1:
                        isBuyer = True
                    if comment['TransactionTypeId_id'] == 2:
                        isSeller = True
            if isSeller and isBuyer:
                brokers.append(user.FacebookId)
        result = []
        for broker in reversed(brokers):
            brokerObj = {'facebookId': broker}
            result.append(brokerObj)
        return JsonResponse(result,safe=False)
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def getUserDetail(request):
    try:
        # facebookId = request.GET['facebookId']
        facebookId = request.data['facebookId']   
        result = {}
        result['facebookId'] = facebookId
        user = Users.objects.get(FacebookId = facebookId)
        posts = []
        products = []
        for post in user.posts_set.values():
            postObj = { 'content': post['Content']}
            posts.append(postObj)
            product = PostProducts.objects.filter(PostId = post['PostId'])
            if product:
                transactionType = TransactionTypes.objects.get(TransactionTypeId = post['TransactionTypeId_id'])
                for prod in product.values():
                    productObj = {'street': prod['Street'], 'district': prod['District'], 'ward': prod['Ward'], 'city': prod['City'], 'price': prod['Price'], 'area': prod['Area'], 'transactionType':transactionType.Name }
                    products.append(productObj)
        result['posts'] = posts
        comments = []
        for comment in user.comments_set.values():
            commentObj = { 'content': comment['Content']}
            comments.append(commentObj)
            product = CommentProducts.objects.filter(CommentId = comment['CommentId'])
            if product:
                transactionType = TransactionTypes.objects.get(TransactionTypeId = comment['TransactionTypeId_id'])
                for prod in product.values():
                    productObj = {'street': prod['Street'], 'district': prod['District'], 'ward': prod['Ward'], 'city': prod['City'], 'price': prod['Price'], 'area': prod['Area'], 'transactionType':transactionType.Name}
                    products.append(productObj)
        result['products'] = products
        for comment in user.replies_set.values():
            commentObj = { 'content': comment['Content']}
            comments.append(commentObj)
        result ['comments'] = comments
        likeSellPosts = 0
        likeBuyPosts = 0
        likeLeasePosts = 0
        likeRentPosts = 0
        for like in Like.objects.filter(LikeOwnerId = user.UserId):
            if like.PostId.TransactionTypeId.Name == 'Mua nhà':
                likeBuyPosts += 1
            if like.PostId.TransactionTypeId.Name == 'Bán nhà':
                likeSellPosts += 1
            if like.PostId.TransactionTypeId.Name == 'Thuê nhà':
                likeRentPosts += 1
            if like.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                likeLeasePosts += 1
        result ['like'] = {'likeSellPosts':likeSellPosts,'likeBuyPosts':likeBuyPosts,'likeLeasePosts':likeLeasePosts,'likeRentPosts':likeRentPosts}
        taggedSellPosts = 0
        taggedBuyPosts = 0
        taggedLeasePosts = 0
        taggedRentPosts = 0
        for tagged in Tagged.objects.filter(TaggedUserId = user.UserId):
            if tagged.CommentId.PostId.TransactionTypeId.Name == 'Mua nhà':
                taggedBuyPosts += 1
            if tagged.CommentId.PostId.TransactionTypeId.Name == 'Bán nhà':
                taggedSellPosts += 1
            if tagged.CommentId.PostId.TransactionTypeId.Name == 'Thuê nhà':
                taggedRentPosts += 1
            if tagged.CommentId.PostId.TransactionTypeId.Name == 'Cho thuê nhà':
                taggedLeasePosts += 1
        result ['tagged'] = {'taggedSellPosts':taggedSellPosts,'taggedBuyPosts':taggedBuyPosts,'taggedLeasePosts':taggedLeasePosts,'taggedRentPosts':taggedRentPosts}
        # products = []
        # for product in user.posts_set.
        return JsonResponse(result,safe=False)
    except ValueError as e:
        return Response(e.args[0],status.HTTP_400_BAD_REQUEST)