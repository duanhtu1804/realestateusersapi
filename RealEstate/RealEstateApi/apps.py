from django.apps import AppConfig


class RealestateapiConfig(AppConfig):
    name = 'RealEstateApi'
